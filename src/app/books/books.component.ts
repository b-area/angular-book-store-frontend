import { Component, OnInit } from '@angular/core';
import { BooksService } from './books.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  title: string = "";
  price: number = 0;
  author: string = "";

  edit_mode: boolean = false;

  books: any[];

  constructor(private booksService: BooksService) { }

  ngOnInit() {
    this.getAllBooks()
  }

  getAllBooks() {
    this.booksService.getAllBooks().subscribe(
      data => {
        this.books = data;
        //console.log(data)
      },
      error => {
        console.log(error);
      }
    );
  }

  addBook() {
    const book = {
      title: this.title,
      author: this.author,
      price: this.price
    }

    this.booksService.addNewBook(book).subscribe(
      data => {
        console.log("Book has been submitted successfully");
        console.log(data);
        this.getAllBooks();
      },
      error => {
        console.log("Received an error.");
        console.log(error);
      }
    )
  }

  updateBook(book: any){
    this.edit_mode = false;
    /*
    this.booksService.updateBook(book, book._id).subscribe(
      data => {
        alert("Book has been successfully updated!")
      },
      error => {
        alert("Got an error!");
      }
    )*/
  }

  edit(item: any) {
    this.edit_mode = true;
    //this.edit_mode = !this.edit_mode;
  }

}
