import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class BooksService {
  private BACKEND_URL = environment.BACKEND_URL + "/books";

  constructor(private http: HttpClient) { }

  // Retrieving all books
  // from the backend through HTTP GET
  getAllBooks(): Observable<any[]> {
    return this.http.get<any[]>(this.BACKEND_URL);
  }

  // Adding a new book
  addNewBook(book: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }

    return this.http.post(this.BACKEND_URL, book, options);
  }

  // Updating a book given an id
  updateBook(data: any, id: string): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }

    // http://localhost:5050/books/101039393993
    return this.http.put(this.BACKEND_URL + "/" + id, data, options);
  }

}
